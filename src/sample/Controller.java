package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button num0;
    public Button num1;
    public Button num2;
    public Button num3;
    public Button num4;
    public Button num5;
    public Button num6;
    public Button num7;
    public Button num8;
    public Button num9;
    public Label Screen;
    public int a=0,b=0;
    public Button number;

    public void doClick(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        String number = Screen.getText();
        Screen.setText(number.concat(button.getText()));
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        switch (button.getText()){
            case "+":
                a = Integer.parseInt(Screen.getText());
                Screen.setText("");
            case "=":
                b = Integer.parseInt(Screen.getText());
                Screen.setText(String.valueOf(a+b));
        }
    }
}
